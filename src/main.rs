use matrix_sdk::{
    config::SyncSettings,
    matrix_auth::MatrixSession,
    room::Room,
    ruma::events::room::{
        member::StrippedRoomMemberEvent,
        message::{MessageType, OriginalSyncRoomMessageEvent, RoomMessageEventContent},
    },
};

/// Entry point of the app.
#[tokio::main]
async fn main() -> Result<(), Error> {
    // set up logging. Configure the level by changing the env var `RUST_LOG`
    tracing_subscriber::fmt::init();
    let Some(path) = std::env::args().nth(1) else {
        return Err(Error(format!(
            "Syntax: {} <config>",
            std::env::args().next().unwrap()
        )));
    };
    let config = Config::load_from_path(std::path::Path::new(&path))?;
    log::info!("Using endpoint '{}'", config.llm_server_endpoint);

    // The file where the session is persisted.
    let session_file = config.data_dir.join("session");

    let (client, sync_token) = if session_file.exists() {
        restore_session(&config, &session_file).await?
    } else {
        (login(&config, &session_file).await?, None)
    };

    // main sync loop
    sync(&config, client, sync_token, &session_file).await?;
    Ok(())
}

/// Bot app config
#[derive(Debug, Clone, serde::Deserialize)]
struct Config {
    /// URL of the homeserver of the user.
    #[serde(default = "default_homeserver_url")]
    pub homeserver_url: String,
    /// Matrix username
    pub username: String,
    /// Matrix password
    pub password: String,
    /// Directory to persist the bot state
    #[serde(default = "default_data_dir")]
    pub data_dir: std::path::PathBuf,
    /// Passphrase to protect the matrix session db
    #[serde(default)]
    pub db_passphrase: Option<String>,
    /// Matrix device display name for the bot
    #[serde(default = "default_device_display_name")]
    pub device_display_name: String,
    /// Only accept invites from users contained in `accepted_users`.
    /// Accept from all users if set to `None`.
    #[serde(default)]
    pub accepted_users: Option<std::collections::HashSet<String>>,
    /// Only accept invites from users whose homeserver is in `accepted_homeserver`.
    /// Accept from all users if set to `None`.
    #[serde(default)]
    pub accepted_homeservers: Option<std::collections::HashSet<String>>,
    /// If not `None`, greet when joining a room.
    #[serde(default)]
    pub welcome_message: Option<String>,
    /// REST service endpoint to query the LLM server.
    #[serde(deserialize_with = "deserialize_uri")]
    pub llm_server_endpoint: hyper::Uri,
    /// Token to start a talk, e.g. "Q:"
    #[serde(default = "default_start_token")]
    pub start_token: String,
    /// Token to mark the start of an ansert, e.g. "A:"
    #[serde(default = "default_stop_token")]
    pub stop_token: String,
}

fn default_homeserver_url() -> String {
    "matrix.org".into()
}
fn default_data_dir() -> std::path::PathBuf {
    "session_data".into()
}
fn default_device_display_name() -> String {
    "MatrixLlmBot".into()
}
fn default_start_token() -> String {
    "Human".into()
}
fn default_stop_token() -> String {
    "Assistant".into()
}
/// Deserialize a string to [hyper::Uri] including DNS resolution.
fn deserialize_uri<'de, D: serde::Deserializer<'de>>(
    deserializer: D,
) -> Result<hyper::Uri, D::Error> {
    use serde::Deserialize;
    let s = String::deserialize(deserializer)?;
    let uri: hyper::Uri = s
        .parse()
        .map_err(|e| serde::de::Error::custom(format!("{e}")))?;
    Ok(uri)
}

/// Restore a previous session.
async fn restore_session(
    config: &Config,
    session_file: &std::path::Path,
) -> Result<(matrix_sdk::Client, Option<String>), Error> {
    log::info!("Previous session found in '{}'", session_file.display());

    // The session was serialized as JSON in a file.
    let serialized_session = tokio::fs::read_to_string(session_file).await?;
    let SessionData {
        user_session,
        sync_token,
    } = serde_json::from_str(&serialized_session)?;

    // Build the client with the previous settings from the session.
    let client = build_client(
        &config.homeserver_url,
        &config.db_path(),
        config.db_passphrase.as_ref().map(String::as_str),
    )
    .await?;

    log::info!("Restoring session for {}…", user_session.meta.user_id);

    // Restore the Matrix user session.
    client.restore_session(user_session).await?;

    Ok((client, sync_token))
}

/// Login with a new device.
async fn login(
    config: &Config,
    session_file: &std::path::Path,
) -> Result<matrix_sdk::Client, Error> {
    log::info!(
        "No previous session found, logging in to {}",
        config.homeserver_url
    );

    let client = build_client(
        &config.homeserver_url,
        &config.db_path(),
        config.db_passphrase.as_ref().map(String::as_str),
    )
    .await?;
    let matrix_auth = client.matrix_auth();

    matrix_auth
        .login_username(&config.username, &config.password)
        .initial_device_display_name(&config.device_display_name)
        .await?;
    log::info!("Logged in as {}", config.username);

    // Persist the session to reuse it later.
    // This is not very secure, for simplicity. If the system provides a way of
    // storing secrets securely, it should be used instead.
    // Note that we could also build the user session from the login response.
    let user_session = matrix_auth
        .session()
        .expect("A logged-in client should have a session");
    let serialized_session = serde_json::to_string(&SessionData {
        user_session,
        sync_token: None,
    })?;
    tokio::fs::write(session_file, serialized_session).await?;

    log::info!("Session persisted in {}", session_file.to_string_lossy());
    Ok(client)
}

/// Build a new client.
async fn build_client(
    homeserver_url: &str,
    db_path: &std::path::Path,
    db_passphrase: Option<&str>,
) -> Result<matrix_sdk::Client, Error> {
    Ok(matrix_sdk::Client::builder()
        .homeserver_url(homeserver_url)
        // Use the SQLite store (enabled by default). This is the crucial part to
        // persist the encryption setup.
        .sqlite_store(db_path, db_passphrase)
        .build()
        .await?)
}

/// The core sync loop
async fn sync(
    config: &Config,
    client: matrix_sdk::Client,
    initial_sync_token: Option<String>,
    session_file: &std::path::Path,
) -> Result<(), Error> {
    // Propagate config to handlers:
    client.add_event_handler_context(config.clone());
    // React to invites (stripped member state events).
    // Add the event handler before the sync, so this happens also for
    // older messages. All rooms we've
    // already entered won't have stripped states anymore and thus won't fire
    client.add_event_handler(on_stripped_state_member);

    // Enable room members lazy-loading, it will speed up the initial sync a lot
    // with accounts in lots of rooms.
    // See <https://spec.matrix.org/v1.6/client-server-api/#lazy-loading-room-members>.
    let filter = matrix_sdk::ruma::api::client::filter::FilterDefinition::with_lazy_loading();

    let mut sync_settings = SyncSettings::default().filter(filter.into());

    // We restore the sync where we left.
    // This is not necessary when not using `sync_once`. The other sync methods get
    // the sync token from the store.
    if let Some(sync_token) = initial_sync_token {
        sync_settings = sync_settings.token(sync_token);
    }

    // Let's ignore messages before the program was launched.
    // This is a loop in case the initial sync is longer than our timeout. The
    // server should cache the response and it will ultimately take less time to
    // receive.
    loop {
        match client.sync_once(sync_settings.clone()).await {
            Ok(response) => {
                // This is the last time we need to provide this token, the sync method after
                // will handle it on its own.
                sync_settings = sync_settings.token(response.next_batch.clone());
                persist_sync_token(session_file, response.next_batch).await?;
                break;
            }
            Err(error) => {
                log::error!("An error occurred during initial sync: {error}");
                log::error!("Trying again…");
            }
        }
    }

    log::info!("The client is ready! Listening to new messages…");

    // Attach a handler for incoming room messages
    client.add_event_handler(on_room_message);

    // This loops until we kill the program or an error happens.
    client
        .sync_with_result_callback(sync_settings, |sync_result| async move {
            let response = sync_result?;

            // We persist the token each time to be able to restore our session
            persist_sync_token(session_file, response.next_batch).await?;

            Ok(matrix_sdk::LoopCtrl::Continue)
        })
        .await?;

    Ok(())
}

/// Persist the sync token for a future session.
/// Note that this is needed only when using `sync_once`. Other sync methods get
/// the sync token from the store.
async fn persist_sync_token(
    session_file: &std::path::Path,
    sync_token: String,
) -> Result<(), matrix_sdk::Error> {
    let serialized_session = tokio::fs::read_to_string(session_file).await?;
    let mut full_session: SessionData = serde_json::from_str(&serialized_session)?;

    full_session.sync_token = Some(sync_token);
    let serialized_session = serde_json::to_string(&full_session)?;
    tokio::fs::write(session_file, serialized_session).await?;

    Ok(())
}

/// Handler for new stripped room member events
async fn on_stripped_state_member(
    room_member: StrippedRoomMemberEvent,
    client: matrix_sdk::Client,
    room: Room,
    config: matrix_sdk::event_handler::Ctx<Config>,
) {
    if client
        .user_id()
        .is_some_and(|id| room_member.state_key != id)
    {
        // the invite we've seen isn't for us, but for someone else. ignore
        return;
    }

    let sender: &str = room_member.sender.as_ref();
    fn is_valid_user(config: &Config, user: &str) -> bool {
        let Some(sender_homeserver) = user.split(':').last() else {
            log::error!("Could not determine homeserver for userid {user}");
            return false;
        };
        config
            .accepted_users
            .as_ref()
            .map(|users| users.contains(user))
            .unwrap_or(true)
            || config
                .accepted_homeservers
                .as_ref()
                .map(|servers| servers.contains(sender_homeserver))
                .unwrap_or(true)
    }
    if !is_valid_user(&config, sender) {
        log::info!("Rejecting invite from {sender} to {}", room.room_id());
        return;
    }

    // The event handlers are called before the next sync begins, but
    // methods that change the state of a room (joining, leaving a room)
    // wait for the sync to return the new room state so we need to spawn
    // a new task for them.
    tokio::spawn(async move {
        log::info!(
            "Autojoining room {} (invite from {})",
            room.room_id(),
            room_member.sender
        );
        let mut delay = 2;

        while let Err(err) = room.join().await {
            // retry autojoin due to synapse sending invites, before the
            // invited user can join for more information see
            // https://github.com/matrix-org/synapse/issues/4345
            log::info!(
                "Failed to join room {} ({err:?}), retrying in {delay}s",
                room.room_id()
            );

            tokio::time::sleep(tokio::time::Duration::from_secs(delay)).await;
            delay *= 2;

            if delay > 3600 {
                log::error!("Can't join room {} ({err:?})", room.room_id());
                break;
            }
        }
        if let Some(welcome_msg) = &config.welcome_message {
            room.send(RoomMessageEventContent::text_plain(welcome_msg))
                .await
                .unwrap_or_log();
        }
    });
}

trait UnwrapOrLog<R> {
    fn unwrap_or_log(self) -> Option<R>;
}

impl<R, E: std::fmt::Debug> UnwrapOrLog<R> for Result<R, E> {
    fn unwrap_or_log(self) -> Option<R> {
        match self {
            Ok(r) => Some(r),
            Err(err) => {
                log::error!("{err:?}");
                None
            }
        }
    }
}

/// Response stream from a LLM server
struct LlmStream {
    response: hyper::Response<hyper::Body>,
    is_done: bool,
}

impl LlmStream {
    /// Post to the LLM server endpoint and initialize the response as a
    /// text/event-stream stream.
    async fn try_new(
        llm_server_endpoint: &hyper::Uri,
        start_token: &str,
        stop_token: &str,
        question: &str,
    ) -> Result<Self, Error> {
        let payload = PrompRequestPayload {
            prompt: &format!("{start_token}: {question}\n{stop_token}:"),
            stream: true,
            stop: &[start_token],
        };
        let response = post(llm_server_endpoint, &payload).await?;
        if let Some(content_type) = response.headers().get(hyper::header::CONTENT_TYPE) {
            if content_type != "text/event-stream" {
                return Err(Error(format!(
                    "Got unsupported content type from LLM server: {content_type:#?}"
                )));
            }
        }
        Ok(Self {
            response,
            is_done: false,
        })
    }
    /// Try to retrieve the next chunk of the stream.
    /// Return `None` when reaching the end of the stream.
    async fn try_next(&mut self) -> Result<Option<String>, Error> {
        if self.is_done {
            return Ok(None);
        }
        use hyper::body::HttpBody;
        let body = self.response.body_mut();
        let Some(chunk) = body.data().await else {
            return Ok(None);
        };
        let chunk = chunk?;
        let chunk = chunk.strip_prefix("data:".as_bytes()).unwrap_or(&chunk);
        let mut data: std::collections::HashMap<String, JsonType> = serde_json::from_slice(chunk)?;
        if let Some(JsonType::Bool(true)) = data.get("stop") {
            self.is_done = true;
        }
        let Some(JsonType::Text(content)) = data.remove("content") else {
            return Err(Error(
                "Response does not contain a text field 'content'".into(),
            ));
        };
        Ok(Some(content))
    }
}

/// Payload sent to a LLM server
#[derive(Debug, serde::Serialize)]
struct PrompRequestPayload<'a> {
    prompt: &'a str,
    stream: bool,
    stop: &'a [&'a str],
}

/// Handler to react to room messages sent to the bot.
async fn on_room_message(
    event: OriginalSyncRoomMessageEvent,
    client: matrix_sdk::Client,
    mut room: Room,
    config: matrix_sdk::event_handler::Ctx<Config>,
) {
    // Only want regular text messages from rooms, the bot is still in
    let MessageType::Text(text_content) = event.content.msgtype else {
        return;
    };
    if client
        .user_id()
        .map(|user_id| user_id == event.sender)
        .unwrap_or(false)
    {
        return;
    }
    log::info!(
        "Received message: room={}, sender={}, body={}",
        room.room_id(),
        event.sender,
        text_content.body
    );

    room.typing_notice(true).await.unwrap_or_log();

    let Some(stream) = LlmStream::try_new(
        &config.llm_server_endpoint,
        &config.start_token,
        &config.stop_token,
        &text_content.body,
    )
    .await
    .unwrap_or_log() else {
        return;
    };

    stream_to_room(stream, &mut room).await.unwrap_or_log();
    room.typing_notice(false).await.unwrap_or_log();
}

async fn stream_to_room(mut stream: LlmStream, room: &mut Room) -> Result<(), Error> {
    // Initially empty message - to be filled with junks received by the LLM stream:
    let mut msg = String::from("");
    let result = room.send(RoomMessageEventContent::text_plain(&msg)).await?;
    while let Some(response) = stream.try_next().await? {
        room.typing_notice(true).await.unwrap_or_log();
        msg += &response;
        room.send(RoomMessageEventContent::text_plain(&msg).make_replacement(
            matrix_sdk::ruma::events::room::message::ReplacementMetadata::new(
                result.event_id.clone(),
                None,
            ),
            None,
        ))
        .await?;
    }
    Ok(())
}

/// The data needed to re-build a client.
#[derive(Debug, serde::Serialize, serde::Deserialize)]
struct ClientSession {
    /// Passphrase of the database.
    passphrase: String,
}

/// The full session to persist.
#[derive(Debug, serde::Serialize, serde::Deserialize)]
struct SessionData {
    /// Matrix user session.
    user_session: MatrixSession,
    /// Latest sync token.
    ///
    /// It is only needed to persist it when using `Client::sync_once()` and we
    /// want to make our syncs faster by not receiving all the initial sync
    /// again.
    #[serde(skip_serializing_if = "Option::is_none")]
    sync_token: Option<String>,
}

impl Config {
    fn load_from_path(path: &std::path::Path) -> Result<Config, Error> {
        let content = std::fs::read_to_string(path)?;
        Ok(toml::from_str(&content)
            .map_err(|err| format!("Syntax error in file {}: {}", path.display(), err))?)
    }

    fn db_path(&self) -> std::path::PathBuf {
        self.data_dir.join("db")
    }
}

/// Minimalistic error handling
struct Error(String);
impl<D: std::fmt::Display> From<D> for Error {
    fn from(e: D) -> Self {
        Error(format!("{}", e))
    }
}
impl std::fmt::Debug for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.pad(self.0.as_str())
    }
}

/// Rust representation of generic json data
#[derive(Debug, serde::Deserialize)]
#[serde(untagged)]
enum JsonType {
    Bool(bool),
    Number(f64),
    Integer(i64),
    Text(String),
    List(Vec<JsonType>),
    Dict(std::collections::HashMap<String, JsonType>),
    #[serde(rename = "null")]
    Null,
}

/// HTTP post request to `url` with payload `payload`
async fn post(
    url: &hyper::Uri,
    payload: &impl serde::Serialize,
) -> Result<hyper::Response<hyper::Body>, Error> {
    let client = hyper::Client::new();
    log::info!("Querying {}", url);
    let request = hyper::Request::builder()
        .method(hyper::Method::POST)
        .header(hyper::header::CONTENT_TYPE, "application/json")
        .uri(url)
        .body(serde_json::to_string(payload)?.into())?;
    let response = client.request(request).await?;
    if !response.status().is_success() {
        let status = response.status();
        let body = hyper::body::to_bytes(response).await?;
        return Err(Error(format!("{status}: {body:?}")));
    }
    Ok(response)
}
