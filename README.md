# matrix-llm-bot

A small matrix bot which redirects messages from rooms to a llm via an
API.
It currently only works together with the [server](https://github.com/ggerganov/llama.cpp/tree/master/examples/server) of [llama.cpp](https://github.com/ggerganov/llama.cpp).


## Compile

Requirements: rust 🦀.

```sh
cargo build --release
```

## Usage

* Clone / compile [llama.cpp](https://github.com/ggerganov/llama.cpp).
  Alternatively use the containerized version:
  `registry.ethz.ch/sis/container/llama.cpp-server/llama.cpp:main`
  (repo: [gitlab.ethz.ch/sis/container/llama.cpp-server](https://gitlab.ethz.ch/sis/container/llama.cpp-server)).
* Get a llama gguf model (e.g. from [🤗 Hugging
  Face](https://huggingface.co/): 🦙 [vicuna-13b-v1.5.Q3_K_M.gguf](https://huggingface.co/TheBloke/vicuna-13B-v1.5-GGUF/blob/main/vicuna-13b-v1.5.Q3_K_M.gguf)).
* Start the *llama.cpp* server:
  ```sh
  /path-to-llama-cpp/server -m model.gguf
  ```
* Prepare a file `config.toml`:

  ```toml
  homeserver_url = "https://matrix.org"
  username = "@chucknorris:matrix.org"
  password = "🔑"
  data_dir = "./private/store/chucknorris"
  device_display_name = "matrix-llm-bot-chucknorris"
  # Setting accepted_users to None (allow all users) by commenting it out:
  # accepted_users = []
  accepted_homeservers = ["matrix.org"]
  welcome_message = "You do not ask questions to the Chuck Norris bot - it asks you."
  llm_server_endpoint = "http://127.0.0.1:8080/completion"
  ```

* Run the bot:

  ```sh
  ./target/release/matrix-llm-bot config.toml
  ```
  
  or use docker 🐋 / podman 🦭:
  
  ```sh
  docker run -it --rm -v ./config.toml:/etc/matrix-llm-bot/config.toml registry.ethz.ch/sis/experimental/matrix-llm-bot/matrix-llm-bot
  ```

* Invite the bot user to a room and start chatting.

## Roadmap

- [ ] Support a LLM context
- [ ] Markdown support (Code Llama)
- [ ] Load balancing / load limiting
- [ ] Try out [github.com/rustformers/llm](https://github.com/rustformers/llm)
