# k8s deployment

The provided example config
[base/files/config.toml](base/files/config.toml) wont work - you have
to create an overlay to match your needs.

```
k8s
├── base
│   └── ...
├── components
│   └── ...
└── overlays
    └── custom
        ├── files
        │   └── config.toml
        └── kustomization.yml
```

An example of `overlays/custom/kustomization.yml` which uses PVCs,
sets a namespace and overrides `config.toml`:

```yml
---
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization

namespace: custom-matrix-llm-bot

resources:
  - ../../base/

components:
  - ../../components/storage/
  - ../../components/storage/pvc/

secretGenerator:
  - name: bot-config
    behavior: replace
    files:
      - files/config.toml
```

Deploy

```sh
kubectl apply -k overlays/custom
```
